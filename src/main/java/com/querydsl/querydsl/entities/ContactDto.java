package com.querydsl.querydsl.entities;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name="CONTACT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ContactDto {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="ID")
    private long id;

    @Column(name="FIRSTNAME")
    private String firstName;

    @Column(name="FULLNAME")
    private String fullName;

    @Column(name="CHECK_MAPPING")
    private String checMap;



}
