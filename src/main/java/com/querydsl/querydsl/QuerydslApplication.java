package com.querydsl.querydsl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

//@EnableJpaRepositories(basePackages = "com.querydsl.querydsl.services.daorepositories")
//@EnableMongoRepositories(basePackages	 = "com.querydsl.querydsl.services.mongorepository")
@SpringBootApplication
//@EnableJpaRepositories(repositoryFactoryBeanClass = DataTablesRepositoryFactoryBean.class)
public class QuerydslApplication {

	public static void main(String[] args) {

		SpringApplication.run(QuerydslApplication.class, args);
		System.out.println("teste new feature 2");
	}


}
