package com.querydsl.querydsl.MongoEntitie;

import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;



@Document(collection = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ContactMongo {
    @Id
    private String id;
    private String firstName;
    private int age;

}
