package com.querydsl.querydsl.services;

import com.querydsl.core.types.Predicate;
import com.querydsl.querydsl.entities.Contact;
import com.querydsl.querydsl.services.daorepositories.ContactDaoRepository;
import com.querydsl.querydsl.services.interfaces.ContactService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@Slf4j
public class ContactServiceImpl implements ContactService {


    private final ContactDaoRepository contactDaoRepository;

    public ContactServiceImpl(ContactDaoRepository contactDaoRepository) {

        this.contactDaoRepository = contactDaoRepository;

    }

    @Override
    public ContactDaoRepository getDaoRepository() {

        return contactDaoRepository;

    }

    @Override
    public List<Contact> getContactAddressBook(Predicate predicate) {

        return contactDaoRepository.findAll(predicate);

    }
}
