package com.querydsl.querydsl.services.daorepositories;


import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;


@NoRepositoryBean
public interface GenericDaoRepository<T, K extends Serializable>
        extends JpaRepository<T, K>, QuerydslPredicateExecutor<T> {


    List<T> findAll(Predicate predicate);

}
