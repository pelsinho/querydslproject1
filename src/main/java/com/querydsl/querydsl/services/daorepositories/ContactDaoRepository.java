package com.querydsl.querydsl.services.daorepositories;


import com.querydsl.querydsl.entities.Contact;
import org.springframework.stereotype.Repository;


/**
 * // * The persistent class for the FCT_ACAO_PREVENCAO_EXEC0 database table.
 * // *
 * //
 */

@Repository
public interface ContactDaoRepository extends GenericDaoRepository<Contact, Long> {


}




