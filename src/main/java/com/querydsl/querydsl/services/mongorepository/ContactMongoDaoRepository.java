package com.querydsl.querydsl.services.mongorepository;


import com.querydsl.querydsl.MongoEntitie.ContactMongo;
import org.springframework.stereotype.Repository;


/**
 * // * The persistent class for the FCT_ACAO_PREVENCAO_EXEC0 database table.
 * // *
 * //
 */

@Repository
public interface ContactMongoDaoRepository extends GenericMongoDaoRepository<ContactMongo, String> {


}




