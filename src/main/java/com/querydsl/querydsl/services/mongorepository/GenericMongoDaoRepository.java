package com.querydsl.querydsl.services.mongorepository;


import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;


@NoRepositoryBean
public interface GenericMongoDaoRepository<T, K extends Serializable>
        extends MongoRepository<T, K>, QuerydslPredicateExecutor<T> {

    List<T> findAll(Predicate predicate);

    List<T> findByFirstName (String firstName);


}
