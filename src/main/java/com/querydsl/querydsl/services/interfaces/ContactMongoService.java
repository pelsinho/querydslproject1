package com.querydsl.querydsl.services.interfaces;


import com.querydsl.core.types.Predicate;
import com.querydsl.querydsl.MongoEntitie.ContactMongo;
import com.querydsl.querydsl.services.mongorepository.ContactMongoDaoRepository;

import java.util.List;

public interface ContactMongoService extends GenericMongoServiceRepository<ContactMongoDaoRepository> {


    List<ContactMongo> getContactAddressBook(Predicate predicate);
}
