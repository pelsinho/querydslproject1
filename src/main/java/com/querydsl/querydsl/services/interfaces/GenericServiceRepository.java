package com.querydsl.querydsl.services.interfaces;

public interface GenericServiceRepository<T> {

    T getDaoRepository();

}
