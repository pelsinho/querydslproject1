package com.querydsl.querydsl.services.interfaces;


import com.querydsl.core.types.Predicate;
import com.querydsl.querydsl.entities.Contact;
import com.querydsl.querydsl.services.daorepositories.ContactDaoRepository;


import java.util.List;

public interface ContactService extends GenericServiceRepository<ContactDaoRepository> {


    List<Contact> getContactAddressBook(Predicate predicate);
}
