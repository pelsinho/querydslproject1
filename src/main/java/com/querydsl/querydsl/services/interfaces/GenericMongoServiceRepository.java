package com.querydsl.querydsl.services.interfaces;

public interface GenericMongoServiceRepository<T> {

    T getDaoRepository();

}
