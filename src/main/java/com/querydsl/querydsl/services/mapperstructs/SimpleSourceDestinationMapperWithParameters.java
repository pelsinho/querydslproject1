package com.querydsl.querydsl.services.mapperstructs;

import com.querydsl.querydsl.entities.Contact;
import com.querydsl.querydsl.entities.ContactDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring") //Use Autowired
public interface SimpleSourceDestinationMapperWithParameters {

    SimpleSourceDestinationMapperWithParameters MAPPER = Mappers.getMapper( SimpleSourceDestinationMapperWithParameters.class );

    @Mapping(target="checMap", source="aaaaa")
    ContactDto sourceToDestination(Contact source);

    @Mapping(target="aaaaa", source="checMap")
    Contact destinationToSource(ContactDto destination);
}

