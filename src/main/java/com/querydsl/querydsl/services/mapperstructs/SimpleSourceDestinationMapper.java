package com.querydsl.querydsl.services.mapperstructs;

import com.querydsl.querydsl.entities.Contact;
import com.querydsl.querydsl.entities.ContactDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring") //Use Autowired
public interface SimpleSourceDestinationMapper {

    SimpleSourceDestinationMapper MAPPER = Mappers.getMapper( SimpleSourceDestinationMapper.class );

    ContactDto sourceToDestination(Contact source);
    Contact destinationToSource(ContactDto destination);
}

