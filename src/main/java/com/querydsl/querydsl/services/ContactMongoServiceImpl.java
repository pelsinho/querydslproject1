package com.querydsl.querydsl.services;

import com.querydsl.core.types.Predicate;
import com.querydsl.querydsl.MongoEntitie.ContactMongo;
import com.querydsl.querydsl.services.mongorepository.ContactMongoDaoRepository;
import com.querydsl.querydsl.services.interfaces.ContactMongoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@Slf4j
public class ContactMongoServiceImpl implements ContactMongoService {


    private final ContactMongoDaoRepository contactMongoDaoRepository;

    public ContactMongoServiceImpl(ContactMongoDaoRepository contactMongoDaoRepository) {

        this.contactMongoDaoRepository = contactMongoDaoRepository;

    }

    @Override
    public ContactMongoDaoRepository getDaoRepository() {

        return contactMongoDaoRepository;

    }

    @Override
    public List<ContactMongo> getContactAddressBook(Predicate predicate) {

        return null;//contactMongoDaoRepository.findAll(predicate);

    }
}
