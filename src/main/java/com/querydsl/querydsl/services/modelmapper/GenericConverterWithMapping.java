package com.querydsl.querydsl.services.modelmapper;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;

public class GenericConverterWithMapping {


    public static <T, E> E mapperWithMapping(T source, Class<E> typeDestination, PropertyMap<T, E> mapping) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        modelMapper.addMappings(mapping);
        return modelMapper.map(source, typeDestination);

    }
}
