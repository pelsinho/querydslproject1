package com.querydsl.querydsl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.querydsl.MongoEntitie.QContactMongo;
import com.querydsl.querydsl.entities.Contact;
import com.querydsl.querydsl.MongoEntitie.ContactMongo;
import com.querydsl.querydsl.entities.QContact;
import com.querydsl.querydsl.services.interfaces.ContactMongoService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {QuerydslApplication.class})
@ContextConfiguration
public class QuerydslMongoApplicationTests {


	@Autowired
	private ContactMongoService contactService;

	ContactMongo contactMongo;

	//you are going to recieve a contact object.
	//to perform pe
	// search in database
	//but  you dont know wich contact fields you are going to received with data



	@Test
	public void findAll(){

		List<ContactMongo> all = contactService.getDaoRepository().findAll();
		System.out.println(all.toString());

	}


	@Before
	public void init(){

		contactMongo = new ContactMongo();
		contactMongo.setId("1");
		contactMongo.setAge(1);
		contactMongo.setFirstName("Nelson");

		contactService.getDaoRepository().save(contactMongo);


		contactMongo = new ContactMongo();
		contactMongo.setId("2");
		contactMongo.setAge(2);
		contactMongo.setFirstName("Joao");

		contactService.getDaoRepository().save(contactMongo);

	}





	//Contact field only with First Name filled
	@Test
	public void findContactByFirstName(){


		Contact contact = new Contact();
		contact.setFirstName("NELS");

		QContactMongo qDataEntity = QContactMongo.contactMongo;
		BooleanBuilder booleanBuilder = new BooleanBuilder();

		if(!checkIfNullOrEmpty(contact.getFirstName()))
			booleanBuilder.and(qDataEntity.firstName.startsWithIgnoreCase(contact.getFirstName()));
		if(!checkIfNullOrEmpty(contact.getFullName()))
			booleanBuilder.or(qDataEntity.age.eq(10));

		//Phone
		//Company
		//Department

		List<ContactMongo> all = contactService.getDaoRepository().findAll();
		System.out.println(all.toString());
		all.forEach(x -> log.info(x.toString()));


		List<ContactMongo> all2 = contactService.getDaoRepository().findAll(booleanBuilder.getValue());
		System.out.println(all2.toString());
		all2.forEach(x -> log.info(x.toString()));

	}


	@Test
	public void findContactByFullName(){


		List<ContactMongo> byFirstName = contactService.getDaoRepository().findByFirstName(contactMongo.getFirstName());

		System.out.println(byFirstName.toString());
		byFirstName.forEach(x -> log.info(x.toString()));

	}






	public static boolean checkIfNullOrEmpty(Object obj) {
		boolean conditionA;
		boolean conditionB;
		// 01. tratar de objectos nulos
		conditionA = (obj == null);
		if(conditionA) return true;
		// 02. tratar de arrays
		conditionA = (obj.getClass().isArray());
		conditionB = (conditionA && ((Object[])obj).length == 0);
		if(conditionB) return true;
		// 03. tratar de strings
		conditionA = (obj instanceof String);
		conditionB = (conditionA && "".equalsIgnoreCase((String)obj));
		if(conditionB) return true;
		// 04. tratar de colecções: List, Vector, ... tudo o que derive de java.util.Collection
		conditionA = (obj instanceof Collection);
		conditionB = (conditionA && ((Collection<?>)obj).isEmpty());
		if(conditionB) return true;
		// 05. tratar de colecções: Map... tudo o que derive de java.util.Collection
		conditionA = (obj instanceof Map);
		conditionB = (conditionA && ((Map<?,?>)obj).size() == 0);

		return conditionB;
	}

}
