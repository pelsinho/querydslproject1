package com.querydsl.querydsl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.querydsl.entities.Contact;
import com.querydsl.querydsl.entities.QContact;

import com.querydsl.querydsl.services.interfaces.ContactService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {QuerydslApplication.class})
@ContextConfiguration
public class QuerydslApplicationTests {


	@Autowired
	private ContactService contactService;


	//you are going to recieve a contact object.
	//to perform pe
	// search in database
	//but  you dont know wich contact fields you are going to received with data











	//Contact field only with First Name filled
	@Test
	public void findContactByFirstName(){


		Contact contact = new Contact();
		contact.setFirstName("NELS");

		QContact qDataEntity = QContact.contact;
		BooleanBuilder booleanBuilder = new BooleanBuilder();

		if(!checkIfNullOrEmpty(contact.getFirstName()))
			booleanBuilder.and(qDataEntity.firstName.startsWithIgnoreCase(contact.getFirstName()));
		if(!checkIfNullOrEmpty(contact.getFullName()))
			booleanBuilder.or(qDataEntity.fullName.endsWithIgnoreCase(contact.getFullName()));

		//Phone
		//Company
		//Department

		List<Contact> all = contactService.getDaoRepository().findAll(booleanBuilder.getValue());
		all.forEach(x -> log.info(x.toString()));

	}


	@Test
	public void findContactByFullName(){

		Contact contact = new Contact();
		contact.setFullName("ES");

		QContact qDataEntity = QContact.contact;
		BooleanBuilder booleanBuilder = new BooleanBuilder();

		if(!checkIfNullOrEmpty(contact.getFirstName()))
			booleanBuilder.and(qDataEntity.firstName.startsWithIgnoreCase(contact.getFirstName()));
		if(!checkIfNullOrEmpty(contact.getFullName()))
			booleanBuilder.and(qDataEntity.fullName.endsWithIgnoreCase(contact.getFullName()));

		List<Contact> all = contactService.getDaoRepository().findAll(booleanBuilder.getValue());
		all.forEach(x -> log.info(x.toString()));

	}



	@Test
	public void findContactByFirstNameAndFullName(){

		Contact contact = new Contact();
		contact.setFirstName("NELS");
		contact.setFullName("ES");

		QContact qDataEntity = QContact.contact;
		BooleanBuilder booleanBuilder = new BooleanBuilder();

		if(!checkIfNullOrEmpty(contact.getFirstName()))
			booleanBuilder.and(qDataEntity.firstName.startsWithIgnoreCase(contact.getFirstName()));
		if(!checkIfNullOrEmpty(contact.getFullName()))
			booleanBuilder.and(qDataEntity.fullName.endsWithIgnoreCase(contact.getFullName()));


		List<Contact> all = contactService.getDaoRepository().findAll(booleanBuilder.getValue());
		all.forEach(x -> log.info(x.toString()));

	}



	public static boolean checkIfNullOrEmpty(Object obj) {
		boolean conditionA;
		boolean conditionB;
		// 01. tratar de objectos nulos
		conditionA = (obj == null);
		if(conditionA) return true;
		// 02. tratar de arrays
		conditionA = (obj.getClass().isArray());
		conditionB = (conditionA && ((Object[])obj).length == 0);
		if(conditionB) return true;
		// 03. tratar de strings
		conditionA = (obj instanceof String);
		conditionB = (conditionA && "".equalsIgnoreCase((String)obj));
		if(conditionB) return true;
		// 04. tratar de colecções: List, Vector, ... tudo o que derive de java.util.Collection
		conditionA = (obj instanceof Collection);
		conditionB = (conditionA && ((Collection<?>)obj).isEmpty());
		if(conditionB) return true;
		// 05. tratar de colecções: Map... tudo o que derive de java.util.Collection
		conditionA = (obj instanceof Map);
		conditionB = (conditionA && ((Map<?,?>)obj).size() == 0);

		return conditionB;
	}

}
