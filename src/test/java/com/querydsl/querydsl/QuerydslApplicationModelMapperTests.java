package com.querydsl.querydsl;

import com.querydsl.querydsl.entities.Contact;
import com.querydsl.querydsl.entities.ContactDto;
import com.querydsl.querydsl.services.modelmapper.GenericConverter;
import com.querydsl.querydsl.services.interfaces.ContactService;
import com.querydsl.querydsl.services.modelmapper.GenericConverterWithMapping;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {QuerydslApplication.class})
@ContextConfiguration
public class QuerydslApplicationModelMapperTests {


	@Autowired
	private ContactService contactService;


	//you are going to recieve a contact object.
	//to perform a search in database
	//but  you dont know wich contact fields you are going to received with data











	//Contact field only with First Name filled
	@Test
	public void findContactByFirstName(){


		Contact contact = new Contact();
		contact.setId(1L);
		contact.setFirstName("Nelson");
		contact.setFullName("Gomes");
		contact.setAaaaa("teste no mapping");


		ContactDto contactDto = GenericConverter.mapper(contact, ContactDto.class);

		System.out.println("contactDto Id. = " + contactDto.getId());
		System.out.println("contactDto FirstName. = " + contactDto.getFirstName());
		System.out.println("contactDto Full Name. = " + contactDto.getFullName());
		System.out.println("contactDto UnMapped. = " + contactDto.getChecMap());


	}





	@Test
	public void findContactByFullNameWithPropertyMap(){

		Contact contact = new Contact();
		contact.setId(1L);
		contact.setFirstName("Nelson");
		contact.setFullName("Gomes");
		contact.setAaaaa("teste with mapping");


		PropertyMap pp = new PropertyMap<Contact, ContactDto>() {
			@Override
			protected void configure() {
				map().setChecMap(source.getAaaaa());
			}
		};


		ContactDto contactDto = GenericConverterWithMapping.mapperWithMapping(contact, ContactDto.class, pp);

		System.out.println("contactDto Id. = " + contactDto.getId());
		System.out.println("contactDto FirstName. = " + contactDto.getFirstName());
		System.out.println("contactDto FullName. = " + contactDto.getFullName());
		System.out.println("contactDto CheckMap. = " + contactDto.getChecMap());


	}



}
